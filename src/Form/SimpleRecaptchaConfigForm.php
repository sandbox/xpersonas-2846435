<?php

namespace Drupal\simple_recaptcha\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SimpleRecaptchaConfigForm.
 *
 * @package Drupal\simple_recaptcha\Form
 */
class SimpleRecaptchaConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simple_recaptcha.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_recaptcha_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('simple_recaptcha.settings');
    $form['site_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site key'),
      '#description' => $this->t('Obtain your site key from Google.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('siteKey'),
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#description' => $this->t('Obtain your secrety key from Google.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('secretKey'),
    ];
    $form['enabled_forms'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enabled forms'),
      '#description' => $this->t('Enabled forms will have Google reCaptcha integrated at the end of each form. Separate form ids with a comma or new line.'),
      '#default_value' => $config->get('enabledForms'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('simple_recaptcha.settings');
    $config->set('siteKey', $form_state->getValue('site_key'));
    $config->set('secretKey', $form_state->getValue('secret_key'));
    $config->set('enabledForms', $form_state->getValue('enabled_forms'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
